# Cave of Code #

Blog for awesome blog posts on programming etc.

## Theme setup

Edit `lib/setup.php` to enable or disable theme features, setup navigation menus, post thumbnail sizes, post formats, and sidebars.

## Developer

### Available gulp commands

* `gulp` — Compile and optimize the files in your assets directory
* `gulp watch` — Compile assets when file changes are made
* `gulp --production` — Compile assets for production (no source maps).

**Note!! Allways run `gulp --producion` before deploying to the live server.**

### Development setup

The following steps are nessesary to setup the local development environment.

#### Files
1. Download wordpress to a location of your choise.
2. Clone this repo into the installed wordpress site in the `themes` directory.
3. Navigate to the theme `cd caveofcode`
4. Install [gulp](http://gulpjs.com) and [Bower](http://bower.io/) globally with `npm install -g gulp bower`
5. Run `npm install`
6. Run `bower install`

#### Server
To enable local development use XAMPP.

1. Download from [Official XAMPP](https://www.apachefriends.org/index.html) and install without Bitnami.
2. Go to [XAMPP How-To configure virtual hosts](http://localhost/dashboard/docs/configure-vhosts.html) and follow the instructions.
   Note on step 4. Use the below snippet instead.
   The first host use what is allready there. In the second add path to repo folder.
3. Make sure the Apache server and MySQL database is started and navigate to [localhost](http://localhost/).
4. Click phpMyAdmin and log in with default credentials `root` and empty password. 
5. Create a database with a name of your choice like `coc`.
6. Go to [caveofcode.localhost](http://caveofcode.localhost/) and follow the instructions and use credentials for your local database.
7. Go to Apperance -> Themes and activate the local Cave of Code theme.

*Snippet for XAMPP host configuration*
```
<VirtualHost *:80>
       DocumentRoot "STANDARD DIR"
       ServerName localhost
</VirtualHost>
<VirtualHost *:80>
    DocumentRoot "DIR to wordpress site"
    ServerName caveofcode.localhost
    <Directory "DIR to wordpress site">
        Order deny,allow
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
```

Remember to keep plugins etc. up to date on your local machine according to the once on the live site to see the same results.
The local installation is completely isolated from the live on, so if anything goes wrong it is allways possible to delete the database and start over.

To enable local plugin installation and debug information go to `wp-config.php` and change the following:

Enable debug mode, set `WP_DEBUG` to `true`.

Enable local download (Not FTP) add the following lines.
It might be nessesary to change to file persmissions to `777` on the `wp-content` folder and all sub-files and folder.
```
/**
 * Enable localhost plugin installation
 */
define('FS_METHOD', 'direct');
```