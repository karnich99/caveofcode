(function ($) {
  // Toggle navbar when window resizes above media-query.
  // This prevents collapsed classes form interfering with non-collapsed elements.
  $(function () {
    $(window).on('resize', function () {
      if (window.innerWidth > 768) {
        $('.navbar-collapse').collapse('hide');
      }
    });
  });
})(jQuery);
