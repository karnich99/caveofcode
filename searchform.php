<form method="get" role="search" class="search-form" action="/">
    <div class="form-group has-feedback">
        <label class="sr-only" for="search">Search</label>
        <input id="search" type="search" class="search-field" name="s" placeholder="What do you like?" />
        <span class="glyphicon glyphicon-search form-control-feedback" aria-hidden="true"></span>
    </div>
</form>
