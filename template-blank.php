<?php
/*
 * Template Name: Blank Page
 * Description: This WordPress page template removes all of the special WordPress html from a page so that you can write whatever html you want between the body tags and style it with it's own css file.
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/content', 'page'); ?>
<?php endwhile; ?>
