<article <?php post_class(); ?>>
    <div class="row">
        <div class="row-height">
            <div class="col-sm-2 hidden-xs col-sm-height col-sm-middle">
                <div class="inside">
                    <?php
                        if ( has_post_thumbnail() ) {
                            the_post_thumbnail('thumbnail', array('class' => 'img-responsive'));
                        }
                    ?>
                </div>
            </div>

            <div class="col-sm-10 col-xs-12 col-sm-height">
                <div class="inside">
                    <header>
                        <h2 class="entry-title">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h2>

                        <?php
                            if (get_post_type() === 'post') {
                                get_template_part('templates/entry-meta');
                            }
                        ?>
                    </header>

                    <div class="entry-summary">
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
