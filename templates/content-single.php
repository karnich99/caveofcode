<center>
  <?php the_post_thumbnail(array(200,200), array('class' => 'img-responsive')); ?>
</center>
<?php
    while (have_posts()) : the_post();

    $post_terms = wp_get_object_terms( $post->ID, 'category', array( 'fields' => 'ids' ) );

    // separator between links
    $separator = '&/&';

    // terms exist and no error
    if ( !empty( $post_terms ) && !is_wp_error( $post_terms ) ) {
        $full_categories = '';

        foreach ($post_terms as $key => $value) {
            $categories = explode($separator, get_category_parents( $value, false, $separator ));
            if($categories[0] == 'Tutorials') {
                $full_categories = '<p class="post-category">' . $categories[1] . ' Tutorial</p>';
            }
        }

        echo $full_categories;
    }
?>

  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title inline"><?php the_title(); ?></h1>

      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>

<?php endwhile; ?>
