<div>
    <span class="text-muted">
        <time datetime="<?= get_post_time('c', true); ?>"><?= get_the_date(); ?></time>
        <?= __('by', 'sage'); ?> 
        
        <a href="<?= get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="text-muted">
            <?= get_the_author(); ?>
        </a>
    </span>
</div>
