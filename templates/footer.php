<footer>
    <div class="row">
        <div class="col-sm-9 container-fluid">
            <div class="row">
                <?php dynamic_sidebar('sidebar-footer'); ?>
            </div>
        </div>
        <div class="col-sm-3"></div>
    </div>

    <div class="row">
        <div class="col-sm-12 text-center">
            <p>
                <?php
                    $copyYear = 2016;
                    $curYear = date('Y');

                    $copyString = $copyYear . (($copyYear != $curYear) ? ' - ' . $curYear : '');

                    $copyString .= ' &copy; ';
                    $copyString .= ' <a href="' . get_site_url() . '">' . get_bloginfo() . '</a>';

                    echo $copyString;
                ?>
            </p>
        </div>
    </div>
</footer>