<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="keywords" content="HTML,CSS,JavaScript,Typescript,C#,.NET,WordPress,Umbraco,AngularJS">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <?php wp_head(); ?>
</head>
