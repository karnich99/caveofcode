<header class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <div class="clearfix">
                <a class="navbar-brand" href="<?= esc_url(home_url('/')); ?>">
                    <?php bloginfo('name'); ?>
                </a>
            </div>
            <div class="navbar-toggle text-center">
                <h2 class="collapsed" data-toggle="collapse" data-target="#header-navbar" aria-expanded="false">Menu</h2>
            </div>
        </div>
        <nav class="collapse navbar-collapse" id="header-navbar">
            <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav navbar-right']);
                endif;
            ?>
        </nav>
    </div>
</header>
