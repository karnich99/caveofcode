<?php use Roots\Sage\Titles; ?>

<?php
    if (!is_front_page() && !is_author()) {
        echo '<div class="page-header">';
        echo '<h1>' . Titles\title() . '</h1>';
        echo '</div>';
    }

    if (is_category()) {
        $this_category = get_category($cat);
        echo '<ul class="cats clearfix">';
        wp_list_categories('orderby=id&show_count=0&title_li=&use_desc_for_title=1&show_option_none=&child_of='.$this_category->cat_ID);
        echo '</ul>';
    }
?>
